const express = require('express');
const sessaoRoutes = express.Router();

const sessoesController = require('../controllers/sessoesController');

sessaoRoutes.get("/", (req, res)=> sessoesController.listaSessoes(req, res));

sessaoRoutes.post("/:filmeId", (req, res) => sessoesController.criarSessao(req, res));

sessaoRoutes.get("/id/:sessaoId", (req, res) => sessoesController.sessaoById(req, res));

sessaoRoutes.get("/:id", (req, res)=> sessoesController.listaSessoesFilme(req, res));

sessaoRoutes.patch("/assento/:id", (req, res)=> sessoesController.mudarEstadoAssento(req,res));


module.exports = sessaoRoutes;
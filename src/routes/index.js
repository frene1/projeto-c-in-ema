const Router = require('express');

const filmeRoutes = require("./filmes.routes");
const sessaoRoutes = require("./sessoes.routes");
const usuariosRoutes = require("./usuarios.routes");
const assentosRoutes = require("./assentos.routes");

const router = Router();


router.use("/filmes", filmeRoutes);
router.use("/sessoes", sessaoRoutes);
router.use("/usuarios", usuariosRoutes);
router.use("/assentos", assentosRoutes);

module.exports = router;



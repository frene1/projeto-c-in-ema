const express = require('express');
const usuariosRoutes = express.Router();

const checkUserExists = require('../middlewares/checkUserExistsMiddleware');
const checkUserByEmail = require('../middlewares/checkUserByEmailMiddleware');

const usuariosController = require('../controllers/usuariosController');

//criando usuario
usuariosRoutes.post('/cadastrar', checkUserExists, checkUserByEmail, (request, response)=>usuariosController.criarUsuario(request,response));
usuariosRoutes.post('/login', (request, response) => usuariosController.loginUsuario(request,response));
usuariosRoutes.get('/', (request, response) => usuariosController.getUsuariobyToken(request,response));

module.exports = usuariosRoutes;
const express = require("express");
const filmesRoutes = express.Router();

const filmesController = require("../controllers/filmesController");

filmesRoutes.get("/", (req, res) => filmesController.listaFilmes(req, res));

filmesRoutes.get("/id/:filmeId", (req, res) => filmesController.getFilmeById(req, res));

module.exports = filmesRoutes;

const express = require("express");
const assentosRoutes = express.Router();

const assentosController = require("../controllers/assentosController");

assentosRoutes.get("/:sessaoId", (req, res) => assentosController.listaAssentos(req, res));

module.exports = assentosRoutes;

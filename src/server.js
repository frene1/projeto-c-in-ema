const express = require("express");
const cors = require('cors');
const swaggerUi = require("swagger-ui-express");
const bodyParser = require('body-parser');

const app = express();
const port = 4000;
const swaggerFile = require('./swagger.json');
const createDatabase = require('../scripts/createDataBase');

const router = require('./routes/index');

app.use(cors());
app.use(bodyParser.json());

app.use(express.json());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use(router);

app.listen(port, () => {
    console.log('Server is running!', port);
});

createDatabase();

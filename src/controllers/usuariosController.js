const { Usuario } = require('../models/models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require("uuid")
const { Op } = require("sequelize");

async function criarUsuario(request, response){
    try{
        const dados = request.body;
        dados.senha = await bcrypt.hash(dados.senha, 8);
        const usuario = Usuario.create(
            dados);
        
        return response.status(201).json(usuario);
    }catch(error){
        return response.status(400).json({error:"Não foi possível criar o usuário!"});
    };
};

async function loginUsuario(request, response){
    try{
        const { email, nomeUsuario, senha } = request.body;

        console.log(request.body);

        console.log(email, nomeUsuario, senha);

        const usuario = await Usuario.findOne({
            where: {
                [Op.or]: [
                    { email: email || "" },
                    { nomeUsuario: nomeUsuario || "" }
                ]
            }
        });

        if (usuario === null){
            return response.status(400).json({error:'Não foi possível encontrar o usuário!'});
        }

        if(!(await bcrypt.compare(senha, usuario.senha))){
            return response.status(400).json({error:'Senha incorreta!'});    
        }

        const secretKey = uuidv4();
        const token = jwt.sign({id: usuario.id}, secretKey, {expiresIn:60*60});

        return response.status(200).json({
            token,
            secretKey
        });

    }catch(error){
        return response.status(404).json({error:"Erro ao realizar o login!"});
    };
};

async function getUsuariobyToken(req, res) {
    try {
        const { token, secretKey } = req.query;
        console.log(token);
        console.log(secretKey);

        const decoded = jwt.verify(token, secretKey);
        const usuarioId = decoded.id;

        const usuario = await Usuario.findByPk(usuarioId);

        return res.status(200).json(usuario);

    } catch (error) {
        console.log(error)
        if (error.name === 'TokenExpiredError') {
            return res.status(400).json({ error: "Token expirado" })
        }

        return res.status(400).json({ error: "Erro ao tentar encontrar o usuário" })   
    }
}

module.exports = {
    criarUsuario,
    loginUsuario,
    getUsuariobyToken
};
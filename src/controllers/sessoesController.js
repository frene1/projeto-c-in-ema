const { Sessao, Assento } = require("../models/models");

// retornar todas as sessões em uma lista
async function listaSessoes(req, res) {
    try {
        let sessoes = await Sessao.findAll();

        return res.status(200).json(sessoes);

    } catch (error) {
        return res.status(400).json({ error: "Não foi possível listar as sessoes." })
    };
};

const fileiras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
const colunas = Array.from({length: 18}, (_, i) => i + 1)
async function criarSessao(req, res) {
    try {
        const { filmeId } = req.params;
        const { horario, cidade, bairro, tipo } = req.body;

        const sessao = await Sessao.create({
            horario,
            cidade,
            bairro,
            tipo,
            FilmeId: filmeId
        });

        fileiras.forEach((fileira) => {
            colunas.forEach((coluna) => {
                Assento.create({
                    numero: coluna,
                    fileira: fileira,
                    preco: "1,00",
                    SessaoId: sessao.id
                })
            })
        })

        return res.status(200).json(sessao);
    } catch ( error ) {
        return res.status(400).json({ error: "Ocorreu um erro ao criar a sessão." })
    }
}

// retornar sessão pelo id
async function sessaoById(req, res) {
    const { sessaoId } = req.params;
    try {
        const sessao = await Sessao.findByPk(sessaoId);

        if (!sessao) {
            return res.status(404).json({ error: "Sessão não encontrada." });
        }

        console.log(sessaoId, sessao);

        return res.status(200).json(sessao);
    } catch( error ) {
        return res.status(400).json({ error: "Ocorreu um erro ao encontrar a sessão." })
    }
}
// retornar uma lista de sessões para um filme específico
async function listaSessoesFilme(req, res) {
    let { id } = req.params;
    console.log(id);
    try {
        let sessoes = await Sessao.findAll();
        sessoes = sessoes.filter((sessao)=> sessao.FilmeId == id);
        return res.status(200).json(sessoes);
    } catch (error) {
        return res.status(400).json({error: "Não foi possível listar as sessoes."})
    }
}

// Atualizar o estado de um assento de uma sessão
async function mudarEstadoAssento(req, res) {
    try {
        let { id } = req.params;
        let { fileira, numero, status } = req.body;

        console.log(fileira, numero, status, id);

        status = status.toString().toLowerCase();

        const assento = await Assento.findOne({
            where: {
                fileira,
                numero: parseInt(numero),
                SessaoId: id
            }
        })

        console.log("Assento", assento);

        await assento.update({'status': status});

        return res.status(200).json(assento);


    } catch (error) {
        console.log(error);
        return res.status(400).json({error: "Não foi possível atualizar assento"})
    };
};


module.exports = {
    listaSessoes,
    sessaoById,
    listaSessoesFilme,
    mudarEstadoAssento,
    criarSessao
};

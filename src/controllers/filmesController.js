const { Filme } = require("../models/models");

// Ler filmes
async function listaFilmes(req, res) {
    try {
        const { titulo, genero, classificacao } = req.query;

        let filmes = await Filme.findAll();

        const tituloLowerCase = titulo ? titulo.toString().toLowerCase() : null;
        if (titulo && titulo != "null") {
            console.log(`filtrando por titulo: ${tituloLowerCase}`);
            filmes = filmes.filter((filme) => filme.titulo.toLowerCase().includes(tituloLowerCase));
        }

        const generoLowerCase = genero ? genero.toString().replaceAll(" ", "").toLowerCase() : null;
        if (genero && genero != "null") {
            console.log(`filtrando por genero: ${generoLowerCase}`);
            filmes = filmes.filter((filme) =>
                filme.genero.split(",").map((genero) => genero.replaceAll(" ", "").toLowerCase()).includes(generoLowerCase)
            );
        }

        if (classificacao && classificacao != "null") {
            console.log(`filtrando por classificacao: ${classificacao}`);
            filmes = filmes.filter((filme) => filme.classificacao == classificacao);
        }

        return res.status(200).json(filmes);

    } catch (error) {
        return res.status(400).json({ error: "Não foi possível listar os filmes." });
    }
}

async function getFilmeById(req, res) {
    try {
        const { filmeId } = req.params;

        console.log(filmeId);

        let filme = await Filme.findByPk(filmeId);

        return res.status(200).json(filme);

    } catch (error) {
        return res.status(404).json({ error: "Não foi possível encontrar o filme." });
    }
}

module.exports = {
    listaFilmes,
    getFilmeById
};

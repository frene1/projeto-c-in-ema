const { Assento } = require("../models/models");


async function listaAssentos(req, res) {
    try {
        const { sessaoId } = req.params;
        const assentos = await Assento.findAll({
            where: {
                SessaoId: sessaoId
            }
        });
        
        return res.status(200).json(assentos);
    } catch ( error ) {
        return res.status(400).json({ error: "Um erro ocorreu ao buscar os assentos" });
    }
}

module.exports = {
    listaAssentos
}

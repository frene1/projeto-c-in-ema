const { DataTypes } = require("sequelize");
const { v4: uuidv4 } = require("uuid")
const sequelize = require("../../config/database");


const Filme = sequelize.define("Filme", {
    id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        allowNull: false,
        primaryKey: true
    },
    titulo: {
        type: DataTypes.STRING,
    },
    urlImage: {
        type: DataTypes.STRING,
    },
    genero: {
        type: DataTypes.STRING,
    },
    classificacao: {
        type: DataTypes.INTEGER,
    },
    diretor: {
        type: DataTypes.STRING,
    },
    descricao: {
        type: DataTypes.STRING
    }
})

const Sessao = sequelize.define("Sessao", {
    id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        allowNull: false,
        primaryKey: true
    },
    horario: {
        type: DataTypes.TIME
    },
    cidade: {
        type: DataTypes.STRING
    },
    bairro: {
        type: DataTypes.STRING
    },
    tipo: {
        type: DataTypes.INTEGER
    }
})

const Assento = sequelize.define("Assento", {
    id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        allowNull: false,
        primaryKey: true
    },
    numero: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    fileira: {
        type: DataTypes.STRING,
        allowNull: false
    },
    preco: {
        type: DataTypes.FLOAT,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        defaultValue: "disponivel",
        allowNull: false
    }
})

const Usuario = sequelize.define("Usuario", {
    id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        allowNull: false,
        primaryKey: true
    },
    nome: {
        type: DataTypes.STRING,
        allowNull: false
    },
    sobrenome: {
        type: DataTypes.STRING
    },
    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    },
    dataNascimento: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nomeUsuario: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    senha: {
        type: DataTypes.STRING,
        allowNull: false
    }
})


Filme.hasMany(Sessao, { onDelete: "CASCADE" });

Sessao.belongsTo(Filme, { foreignKey: "FilmeId" });
Sessao.hasMany(Assento, { onDelete: "CASCADE" });

Assento.belongsTo(Sessao, { foreignKey: "SessaoId" });
Assento.belongsTo(Usuario, { foreignKey: "UsuarioId" });

Usuario.hasMany(Assento, { onDelete: "CASCADE" });

module.exports = { Filme, Sessao, Assento, Usuario }

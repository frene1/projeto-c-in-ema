const {Usuario} = require('../models/models')

async function checkUserExists(request, response, next){

    try{
        const { email } = request.body;
        const usuario = await Usuario.findOne({
            where: {
                email,
            },
        });
        
        if (usuario){ //ver se usuário já existe
            return response.status(400).json({error: "Este email já está cadastrado!"});
        }
    
        return next();

    }catch(error){
        return response.status(500).json({error:'Erro ao checar se o usuário existe'})
    }
}
module.exports = checkUserExists;
const {Usuario} = require('../models/models')

async function checkUserExists(request, response, next){

    try{
        const { nomeUsuario } = request.body;
        const usuario = await Usuario.findOne({
            where: {
                nomeUsuario,
            },
        });
        
        if (usuario){ //ver se usuário já existe
            return response.status(400).json({error: "Este nome de usuário não está disponível!"});
        }
    
        return next();

    }catch(error){
        return response.status(500).json({error:'Erro ao checar se o usuário existe'})
    }
}
module.exports = checkUserExists;